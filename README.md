# CS205_Partner_Project

## How to set up project

Run `pip -r requirements.txt` to install the dependencies.

## File structure

`main.py` - Product script
`TestDriver.py` - Test script
`model.py` - The class and data model

## How to test the project

1. In GitLab CI/CD, you can find the pipeline and all the test scripts.
2. Run the `TestDriver.py file`

## Intro.

In this project, we are going to design an enrollment system. We have course and student class as the data model, the
enrollment class as a record of enrollment after the student enrolls in the course. The registration class is the main
instance to manage all the students and courses.

## Model

## [Diagram](https://viewer.diagrams.net/?tags=%7B%7D&highlight=0000ff&edit=_blank&layers=1&nav=1&title=cs205_partner.drawio#R7Z1bc5s4FMc%2FjWe2D%2B1w8yWPidPLttntJd22%2B8QooNhqMHKFnMT99CuBZMCSbZEYyBJm8gBCXKzfX0dHOgcycKeL%2B7cELOd%2F4RBGA8cK7wfu%2BcBxbM9xBvzPCtdZyXgkCmYEhaJSXnCJfkNRaInSFQphUqpIMY4oWpYLAxzHMKClMkAIvitXu8ZR%2Ba5LMINKwWUAIrX0OwrpPCudOOO8%2FB1Es7m8sz06yY4sgKwsfkkyByG%2BKxS5rwfulGBMs63F%2FRRGvPFku3z%2Fc%2F09urgZvX3%2FOfkF%2Fjn78PXvby%2Bzi72pcsrmJxAY0wdf%2BvfN9Zt337yfP5af3ny%2Bs96eXr1%2FKU6xbkG0Eu31Bc5QQgmgCMfid9O1bMzkDi0iELO9s2sc00txhDXFGYjQLGbbAXtGSFjBLSQUMQ6n4gDFS1YazFEUXoA1XvFfklAQ3Mi9szkm6De7LIjYIZsVsMOECkk5o1KNS34mK7ZYKYEJq%2FNJNo%2B9KboACRV1AhxFYJmgq%2FSBeZUFIDMUn2FK8UJeCK%2FiEIZib8M73aEE32wUxM83hCLg8daA9wVJCkhvIV5AStasiji60ZvocLbcv8vl68reNS9I13VFIRBdZra59uZ2X1gXA%2FGMNUJ%2BP3frfp56P3ukuR8jUrodiBj4GFB4xpsxKYqRbRR%2Bal6USrSCXG1FrgFekQT6IQoG7ik7NBhzVNn2ND02GJ8rOmYoaEGzEbymOxWbLEGA4tlFWufcy0u%2BiKbgRZidex2lapmjMIRxqiYKKMgExyW0xCimaVsNz9gfa9Gp9Wo4GLIHmrJ9O99nf7w6oVMc876IUoVBpuY7yBWt0d7e3n1Ye%2Bsy0kNS20ZfVFqJeVXAjgI4oSvWnFRP%2BDI72EXEe2zRnC4isVmXEIZOy0LwFCEohCOUDkSiNWytkT6Af8FA8stJ3l%2B5HM5f2oomXFUTroZ%2FBK5g9AknKB093XOS1d3SRVu9ezwxgzqpielQYQrC0M8s%2BB8JjK55L2GeILtIukER5QMV32Qc2eNkhwkMEZU1FqICO50NbazV%2FQHXJP9dbNAHHBldb1cBAV0xJyMtRZzBKqCYZPtLAn0Cf604Cl6wuRg3PQ%2F6W4CfmMhrvihdsWM2qy7hTgyHJc97vHI%2FXnyYfaUj2xmRj9PlaHwzOTt%2F6Z7ska6Pr35u5Dt1BqdWVl50Qp4BdgWnqRJ2YrelZ3pwFNrj%2BD6Ku3SFt7gLn6Rgs2JIpdW6XkVRDKRdiuAtFJYmtQNiE8WYvOiFYCqEYetCUGceBSGULAAnmnmlRRe1h20Me9w6bM0sBAISzP3MmPtXax%2BFRY8lAknCinrIxpBPWofs7oIsO%2FU2ZVme2voetSnqzUpwe6h1k0lL6dLpdKPAO93vORtzbt9bGymcgzkMbnxwC1DEW7NosxUfvR%2B8jyCC9j21ySER%2BCgOMCHZWoCBIgpTt14cjxKHqWe3L5DxOHGMFXGQNN4FiboOJcd7ObETs%2F1%2B7Dcn7loNmgPt86lDguzaZqFNa9CHNgfHCm3KKKJcj9aoQxtpHE4eFNm0tyKp7sjwfvVGNrWWSRPZ5PX5OMN7a29wSh26SgBTT%2FgIK8Xax1O9jyx0kZLkccuukTxqnHK0Qxe745QP7NFH8yjUwEAWtcp5F9b9uxsCqKKC4%2Ff6sWmvr0sF0qUpmu8sTJlb8B5%2BPfBty2mOvj4rSR2808B0Z21%2BXdbcdtQMhYY7sooyTxvYpBP0blnVPjr02gari%2Bls5YP0WKtinbTtftlqFKeQ0NNxA1zD9Mlq3ZNSYzXFjKwsA3SYZoDyhuu5mnF1G%2FSR9FzV5L9SXlxP9oFkhw26TNrnU2fACrznnqlb2RF2Job%2B0hFSdfUJj%2Bq6ue%2BjGFHf12XqFtJzC0m7xUxcbdJuMbKmetm7q%2BzL3y2ccMxU3ueTxFuOftRltk4MB6STupLRNUs2vg9%2FMX1jOocdTM7UcD2%2B4XI1eV0NOxo6yzUHyZyR7R7UuvqnZ7c%2BEVBD9TPIg%2FE9RXOKnqGVrY%2BiGg%2FjFLNUux6kMchR6wujqpvPQapuW0%2FVnOpJ26uijuoElalmnnYHmbYbsxpq3ulvmLxqmH2ftVQnvaSWYXsN%2BlL6uY66Si7fnN9G3afB1Z8GN5STJCGQkXEYRU5aq%2BXBuV75fq7ufmMDQTbxhQ91%2FR%2FGhKFdbPKuI5R0NUR30NJsevJjPunRsPFRV%2F4Ves99gbg6Vs0HOppdINYtnykLxLtei85XT9P35fuXpCuoZKckTLObvT2DyOM0oSbPdHtJtTaUtmlmfG3zAxkw1i2j8v7dwzSH2eQQrIep%2Bv98mp87Vj3SikhNh9%2F6kKp%2Bspy%2F9yyrsdR8FqthD1kNdbzO5zz9DL35Gbq79ebY0Pg9E%2BtBM%2FTN9f8P3%2BDUvGCteXO6k%2BbnsK0ZG0vtyczP1ciO7jX3zhE96otr1bk36UHoP7WqOoUK42e%2FLlMZq6kzUVdak6PzC9XEvb2ft%2Biq81gDbs2XKbW43breN3bU1VWx5pLT7ujqy0MS2qoT1n1%2BveGpnjo96PZU70gZqBnr3WCbDMvqwepi8J1eY2uI7Ljt1Rk5nSugg%2BEMyp%2FOWmiOZzgG0eu8dMshzetc4NRl4h7TT0jpWkzGwYrism%2FGGo2sf%2FDzX3neRBb8y90t65VlebLk%2FF7cI9tbF%2Fc%2BQYJYG%2FDVg4L%2Fyx9%2BJ6J9U%2B6EORoB3IdS9AEKyAzuY74jx4LACFB0W3664%2FO0W%2BY5LNK0DoCsndmJIbNdK28NQfNahWaXkOUE64e299Nnh6F5rUIbP6WeZrcMTQZuDkPbMSI2A03%2BS7DWoFmV7GPdA93ej0o%2F7YFu5D6l7tfgQLf3G9FPfKAbtTvQKd2P%2BZrOk%2ByCXrtdkO3m%2Fy0wC8fk%2F3PRff0f)

In the model part, we create four classes: Course, Student, Enrollment, and Registration. The entities are students,
courses, and Enrollment. The relationship between them is that an Enrollment contains a Student object and a courses
object, which is a many-to-many relationship. Registration is the behavior when students search for courses and register
for courses.

For course and student objects, they have basic fields to identify themself. For example, courses have course ID, title,
alias, time, credit, and so on, the student has netid, name, and so on. And the enrollment is a record and when the
student registers for a course successfully, it will store in the student information, which will contain the student
information and course information. The registration is the behavior and in this part, the system will check if the
course is available for the student, for example, some courses have major request and pre-request, so when the student
register the course, the system will check the enrollment in the student to see what courses that student toke before
and to check does this student can register this course. The major request will check the student's major.

And there are the add_course function and add_student function in the registration class. These functions will help the
system to add courses and students, and that information will be stored in the two dictionaries. And there are search
courses and search student functions in registration class, so students can search courses by course title or course id.

And we write the check_available_incorrection function which will be a fail test. For the correct one, because course
pre_request, major_request, and student major, student enrollment, those will be list objects, so the system will
compare course pre_request list and student enrollment list, to see if it is available for registering. And also will
compare major_request list and student major list to see if they match. But in this incorrect implementation function,
then check if the student major match, the student major is not list, the logic check does the student major (str) in
the course major request (list), so it have logic error, and can not get correct result.

## Test

|                        Test                        |                                                                                                                                                                                                         Description                                                                                                                                                                                                          |
|:--------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|                  test_course_one                   |                                                                                                                                                 Test the course class, we create two new courses and a copy of one course, then we test these three courses equal function.                                                                                                                                                  |
|                  test_student_two                  |                                                                                                                                                    Test the student class, creating two new students and a copy of one student, then test their student’s equal function.                                                                                                                                                    |
|               test_enrollment_three                |                                                                                                                                                                                   Test the enrollment class, by testing the equal function                                                                                                                                                                                   |
| test_Registration_and_correct_check_available_four | Test the registration and enrollment class, when students enroll in the course successfully, the registration system will automatically create the enrollment instance. We will test the student register and course register function to make sure the system can manage the courses and students. Also, we will test the enrollment record inside the student by testing the count when the student enrolls in the course. |
|            test_check_available_correct            |                                                                                                                                                We will test the correct function `check_available` which will check whether the student is eligible to take the given course.                                                                                                                                                |
|           test_check_available_Incorrect           |                                                                                                               We will test an incorrect function `check_available_incorrection` of “check available” which will check if the student can enroll in the given course. The incorrect test will raise a failure.                                                                                                                |

## Test result

Total cases: `6`

Pass: `5`

Fail: `1` *The incorrect function test*






