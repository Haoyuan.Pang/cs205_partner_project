"""
Created by Kamiku Xue
CS205 Partner Project
"""
import unittest

import model


class TestDriver(unittest.TestCase):
    # Create the test course
    class_1 = model.Course(cid=1, alias='cs205', title='Software Engineering', credit=3, time='14:20-15:10',
                           selection_capacity=50, selection_actual=47, instructor='Jason Hibbeler',
                           pre_request=['cs120'], major_request=['Computer Science'])
    class_2 = model.Course(cid=2, alias='cs275', title='Mobile App Development', credit=3, time='10:05-11:20',
                           selection_capacity=50, selection_actual=47, instructor='Jason Hibbeler',
                           pre_request=['cs120', 'cs124'], major_request=['Data Science'])

    # Create the test student
    student_1 = model.Student(netid='std1', fullname="Student One", level='Senior', major=['Computer Science'],
                              minor=['Music Technology and Business'])
    student_2 = model.Student(netid='std2', fullname="Student Two", level='Senior',
                              major=['Computer Science', 'Data Science'], minor=['Music'])

    # Init the register system
    register = model.Registration()

    def test_course_one(self):
        """
        This part will test the Course class works with equal function and insert function
        :return: None
        """
        # Have a new course with same info with course 1
        class_1_copy = model.Course(cid=1, alias='cs205', title='Software Engineering', credit=3, time='14:20-15:10',
                                    selection_capacity=50, selection_actual=47, instructor='Jason Hibbeler',
                                    pre_request=['cs120'], major_request=['cs'])

        # Copy course of class 1
        class_3 = self.class_1

        # Test cases
        self.assertEqual(self.class_1, class_1_copy)
        self.assertEqual(self.class_1, class_3)
        self.assertNotEqual(self.class_1, self.class_2)

    def test_student_two(self):
        """
        This part will test student class works with equal function and insert function
        :return: None
        """
        # Have a new student with same info with student 1
        student_1_copy = model.Student(netid='std1', fullname="Student One", level='Senior', major='Computer Science',
                                       minor='Music Technology and Business')

        # Copy student of student 1
        student_3 = self.student_1

        # Test cases
        self.assertEqual(self.student_1, student_1_copy)
        self.assertEqual(self.student_1, student_3)
        self.assertNotEqual(self.student_1, self.student_2)

    def test_enrollment_three(self):
        """
        This part will test enrollment class works with equal function and insert function
        :return: None
        """
        # Sim create tow enrollment record to test
        enroll_1 = model.Enrollment(course=self.class_1, student=self.student_1)
        enroll_2 = model.Enrollment(course=self.class_2, student=self.student_1)

        # Create a new enrollment with same info with enroll_1
        enroll_1_copy = model.Enrollment(course=self.class_1, student=self.student_1)

        # Test cases
        self.assertEqual(enroll_1, enroll_1_copy)
        self.assertNotEqual(enroll_1, enroll_2)

    def test_Registration_and_correct_check_available_four(self):
        """
        This part will test registration class works with equal function and insert function
        :return: None
        """
        # Add course to system
        self.register.add_course_obj(self.class_1)
        self.register.add_course_obj(self.class_2)

        # Add course with same info and different cid (This should only add 1 class to the system)
        self.register.add_course(cid=3, title=self.class_1.title, alias=self.class_1.alias,
                                 credit=self.class_1.credit, time=self.class_1.time,
                                 selection_capacity=self.class_1.selection_capacity,
                                 selection_actual=self.class_1.selection_actual, instructor=self.class_1.instructor,
                                 pre_request=self.class_1.pre_request, major_request=self.class_1.major_request)

        self.register.add_course(cid=4, title=self.class_1.title, alias=self.class_1.alias,
                                 credit=self.class_1.credit, time=self.class_1.time,
                                 selection_capacity=self.class_1.selection_capacity,
                                 selection_actual=self.class_1.selection_actual, instructor=self.class_1.instructor,
                                 pre_request=self.class_1.pre_request, major_request=self.class_1.major_request)

        # Test case
        self.assertEqual(len(self.register.course_dic), 4)

        # Add courses manually
        self.register.add_course_obj(
            model.Course(cid=5, alias='cs120', title='C++', credit=3, time='10:05-11:20',
                         selection_capacity=50, selection_actual=47, instructor='Lisa',
                         pre_request=[], major_request=[]))
        self.register.add_course_obj(
            model.Course(cid=6, title='Algorithm', alias='cs124', credit=3, time='10:05-11:20',
                         selection_capacity=50, selection_actual=47, instructor='Tester',
                         pre_request=[], major_request=[]))

        # Test case
        self.assertEqual(len(self.register.course_dic), 6)

        # Add student to the system (this should only add 1 student into the system)
        self.register.add_student_obj(self.student_1)
        self.register.add_student(netid=self.student_1.netid, fullname=self.student_1.fullname,
                                  level=self.student_1.level,
                                  major=self.student_1.major, minor=self.student_1.minor)
        self.register.add_student(netid=self.student_1.netid, fullname=self.student_2.fullname,
                                  level=self.student_2.level,
                                  major=self.student_2.major, minor=self.student_2.minor)
        self.assertEqual(len(self.register.student_dic), 1)

        # Add another student
        self.register.add_student(netid=self.student_2.netid, fullname=self.student_2.fullname,
                                  level=self.student_2.level,
                                  major=self.student_2.major, minor=self.student_2.minor)
        self.assertEqual(len(self.register.student_dic), 2)

        # Add the student manually
        self.register.add_student('test', 'Tester Test', "Freshman", ['Math'], ['Music Technology & Business'])
        self.assertEqual(len(self.register.student_dic), 3)

        # Test the student search function
        self.assertEqual(self.register.search_student_by_id(student_netid=self.student_1.netid), self.student_1)
        self.assertEqual(self.register.search_student_by_id(student_netid='test').fullname, 'Tester Test')

        # Test the course search function
        self.assertEqual(self.register.search_Course_by_id(self.class_1.cid), self.class_1)
        self.assertIn(self.class_1, self.register.search_Course_by_title(self.class_1.title))
        self.assertEqual(len(self.register.search_Course_by_title(self.class_1.title)), 3)

        # Test enroll function of system
        self.register.register_course(self.student_1.netid, 5)
        self.register.register_course(self.student_1.netid, 6)
        self.assertTrue(self.register.check_available(self.class_1, self.student_1))
        self.assertFalse(self.register.check_available(self.class_1, self.student_2))

        # Test enrollment correct
        self.assertEqual(len(self.register.search_student_by_id(student_netid=self.student_1.netid).enrollment), 2)
        self.assertEqual(len(self.register.search_student_by_id(student_netid=self.student_2.netid).enrollment), 0)

    def test_check_available_correct(self):
        """
        Here we will test correct function check_available
        :return: None
        """
        self.assertTrue(self.register.check_available(self.class_1, self.student_1))
        self.assertFalse(self.register.check_available(self.class_1, self.student_2))

    def test_check_available_Incorrect(self):
        """
        Here we will test an incorrect function
        :return: None
        """
        # This should be true because the student have met the requirement above.
        self.assertTrue(self.register.check_available_incorrection(self.class_1, self.student_1))


if __name__ == '__main__':
    unittest.main()
