from prettytable import PrettyTable

import model


def main():
    register = model.Registration()
    # Create the test course
    class_1 = model.Course(cid=1, alias='cs205', title='Software Engineering', credit=3, time='14:20-15:10',
                           selection_capacity=50, selection_actual=47, instructor='Jason Hibbeler',
                           pre_request=['cs120'], major_request=['Computer Science'])
    class_2 = model.Course(cid=2, alias='cs275', title='Mobile App Development', credit=3, time='10:05-11:20',
                           selection_capacity=50, selection_actual=47, instructor='Jason Hibbeler',
                           pre_request=['cs120', 'cs124'], major_request=['Data Science'])

    print("|-[Course 1 Info]\n", class_1)
    print("|-[Course 2 Info]\n", class_2)

    # Add course to system
    register.add_course_obj(class_1)
    register.add_course_obj(class_2)

    # Add courses manually
    register.add_course_obj(
        model.Course(cid=5, alias='cs120', title='C++', credit=3, time='10:05-11:20',
                     selection_capacity=50, selection_actual=47, instructor='Lisa',
                     pre_request=[], major_request=[]))
    register.add_course_obj(
        model.Course(cid=6, title='Algorithm', alias='cs124', credit=3, time='10:05-11:20',
                     selection_capacity=50, selection_actual=47, instructor='Tester',
                     pre_request=[], major_request=[]))

    # Create the test student
    student_1 = model.Student(netid='std1', fullname="Student One", level='Senior', major=['Computer Science'],
                              minor=['Music Technology and Business'])
    student_2 = model.Student(netid='std2', fullname="Student Two", level='Senior',
                              major=['Computer Science', 'Data Science'], minor=['Music'])
    print("|-[Student 1 Info]\n", student_1)
    print("|-[Student 2 Info]\n", student_1)
    register.add_student_obj(student_1)
    register.add_student_obj(student_2)

    # Add the student manually
    register.add_student('test', 'Tester Test', "Freshman", ['Math'], ['Music Technology & Business'])

    # Output the course and student list inside the system
    out = PrettyTable(class_1.__dict__.keys())
    out.title = "Course List"
    for i in register.course_dic.values():
        out.add_row(i.__dict__.values())
    print("|-[All Courses]\n", out)

    out = PrettyTable(student_1.__dict__.keys())
    out.title = "Student List"
    for i in register.student_dic.values():
        out.add_row(i.__dict__.values())
    print("|-[All Student]\n", out)

    # output the student
    print("|-[Student 1 Info from system]\n", register.search_student_by_id(student_netid=student_1.netid))
    print("|-[Student 2 Info from system]\n", register.search_student_by_id(student_netid=student_2.netid))

    # output the course
    print("|-[Course 1 Info from system by id]\n", register.search_Course_by_id(class_1.cid))
    print("|-[Course 1 Info from system by id]\n", register.search_Course_by_id(class_2.cid))
    print("|-[Course 2 Info from system by title]\n", register.search_Course_by_title(class_2.title)[0])


if __name__ == '__main__':
    main()
