# Classes
import datetime

from prettytable import PrettyTable


# course class
class Course:

    # construction for course class
    def __init__(self, cid, alias, title, credit, time, selection_capacity, selection_actual, instructor, pre_request,
                 major_request):
        self.cid = cid
        self.alias = alias
        self.title = title
        self.credit = credit
        self.time = time
        self.selection_capacity = selection_capacity
        self.selection_actual = selection_actual
        self.instructor = instructor
        self.pre_request = pre_request
        self.major_request = major_request

    def __eq__(self, other):
        return self.cid == other.cid

    def __hash__(self) -> int:
        return hash((self.cid, self.title))

    def get_id(self):
        return self.cid

    def get_title(self):
        return self.title

    def get_selection_capacity(self):
        return self.selection_capacity

    def get_selection_actual(self):
        return self.selection_actual

    def __str__(self):
        out = PrettyTable(self.__dict__.keys())
        out.add_row(self.__dict__.values())
        return out.__str__()


# student class
class Student:
    enrollment: list

    def __init__(self, netid, fullname, level, major, minor):
        self.fullname = fullname
        self.netid = netid
        self.level = level
        self.major = major
        self.enrollment: [Enrollment] = []
        self.minor = minor

    def __eq__(self, other):
        return self.netid == other.netid

    def __hash__(self) -> int:
        return hash((self.netid, self.fullname))

    def get_enrollment(self):
        return [i.course for i in self.enrollment]

    def __str__(self):
        out = PrettyTable(self.__dict__.keys())
        out.add_row(self.__dict__.values())
        return out.__str__()


# Enrollment class
class Enrollment:
    course: Course
    student: Student

    def __init__(self, course, student):
        self.course = course
        self.student = student
        self.create_datetime = datetime.datetime.now()

    def __eq__(self, other):
        return self.course == other.course and self.student == other.student

    def __str__(self):
        out = PrettyTable(self.__dict__.keys())
        out.add_row(self.__dict__.values())
        return out.__str__()

    def __hash__(self):
        return hash((self.course.cid, self.student.netid))


# Registration class
class Registration:
    course_dic: {str: Course} = {}
    student_dic: {str: Student} = {}

    def add_course(self, cid, title, alias, credit, time, selection_capacity, selection_actual, instructor, pre_request,
                   major_request):

        """
        add courses object for the system
        :param cid: cid
        :param title: title
        :param alias: alias
        :param credit: credit
        :param time: time
        :param selection_capacity: selection_capacity
        :param selection_actual: selection_actual
        :param instructor: instructor
        :param pre_request: pre_request
        :param major_request: major_request
        :return: None
        """

        course_add = Course(cid=cid, title=title, alias=alias, credit=credit, time=time,
                            selection_capacity=selection_capacity, selection_actual=selection_actual,
                            instructor=instructor, pre_request=pre_request, major_request=major_request)

        for i in self.course_dic.values():
            if i.cid == course_add.cid:
                return

        self.course_dic[cid] = course_add

    def add_course_obj(self, course: Course):
        """
        different way to add course object for the system
        :param course: course
        :return: None
        """

        for i in self.course_dic.values():
            if i.cid == course.cid:
                return

        self.course_dic[course.cid] = course

    def add_student(self, netid, fullname, level, major, minor):
        """
        add student for the system
        :param netid: netid
        :param fullname: fullname
        :param level: level
        :param major: major
        :param minor: minor
        :return: None
        """
        count = 0
        student_add = Student(netid=netid, fullname=fullname, level=level, major=major, minor=minor)
        for i in self.student_dic.values():

            if i == student_add:
                count += 1

        if count == 0:
            self.student_dic[netid] = Student(netid=netid, fullname=fullname, level=level, major=major, minor=minor)

    def add_student_obj(self, student: Student):
        """
        add student object for system
        :param student: studnet
        :return: None
        """

        count = 0
        for i in self.student_dic.values():
            if i == student:
                count += 1

        if count == 0:
            self.student_dic[student.netid] = student

    def search_Course_by_id(self, class_id):
        """
        search course by id
        :param class_id: class_id
        :return: course object if find
        """
        try:
            return Registration.course_dic[class_id]
        except:
            return "DOES NOT FUND THE CLASS"

    def search_Course_by_title(self, title):
        """
        search course by title
        :param title: title
        :return: course object if find
        """
        try:
            l1 = []
            for i in Registration.course_dic.values():
                if i.get_title() == title:
                    l1.append(i)

            if len(l1) == 0:
                return "DOES NOT FUND THE CLASS"

            else:
                return l1
        except:
            return "DOES NOT FUND THE CLASS"

    def search_student_by_id(self, student_netid):
        """
        search student by netid
        :param student_netid: student_netid
        :return: student object if find
        """
        try:
            return Registration.student_dic[student_netid]
        except:
            return "DOES NOT FUND THE CLASS"

    def check_available(self, course: Course, student: Student):
        """
        to check if the course is available to register by the student
        :param course: course
        :param student: student
        :return: ture if course can be registered by this student, otherwise, false
        """
        count = 0
        if len(course.pre_request) == 0:
            if len(course.major_request) == 0:
                return True
            else:
                if student.major in course.major_request:
                    return True
                else:
                    return False
        else:
            for i in student.get_enrollment():
                if i == course:
                    count += 1

            if count == 0 and len(
                    set(course.pre_request) & set([i.alias for i in student.get_enrollment()])) != 0 and (
                    len(set(student.major) & set(course.major_request)) != 0 or len(
                set(student.minor) & set(course.major_request)) != 0):
                return True
            else:
                return False

    def register_course(self, student_id, course_id):

        course = self.course_dic[course_id]
        student = self.student_dic[student_id]

        if self.check_available(course, student):
            record = Enrollment(course, student)

            student.enrollment.append(record)


        else:
            print("You can not registrant this course")

    def check_available_incorrection(self, course: Course, student: Student):
        """
        incorrect implementation for check available function
        :param course: course
        :param student: student
        :return: ture if course can be registered by this student, otherwise, false
        """
        count = 0
        if len(course.pre_request) == 0:
            if len(course.major_request) == 0:
                return True
            else:
                if student.major in course.major_request:
                    return True
                else:
                    return False
        else:
            for i in student.get_enrollment():
                if i == course:
                    count += 1

            if count == 0 and len(
                    set(course.pre_request) & set([i.alias for i in
                                                   student.get_enrollment()])) != 0 and student.major in course.major_request or student.minor in course.major_request:
                return True
            else:
                return False
